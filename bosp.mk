
ifdef CONFIG_CONTRIB_MANDELBROTMULTITHREAD

# Targets provided by this project
.PHONY: mandelbrotmultithread clean_mandelbrotmultithread

# Add this to the "contrib_testing" target
testing: mandelbrotmultithread
clean_testing: clean_mandelbrotmultithread

MODULE_CONTRIB_USER_MANDELBROTMULTITHREAD=contrib/user/MandelbrotMultiThread

mandelbrotmultithread: external
	@echo
	@echo "==== Building MandelbrotMultiThread ($(BUILD_TYPE)) ===="
	@echo " Using GCC    : $(CC)"
	@echo " Target flags : $(TARGET_FLAGS)"
	@echo " Sysroot      : $(PLATFORM_SYSROOT)"
	@echo " BOSP Options : $(CMAKE_COMMON_OPTIONS)"
	@[ -d $(MODULE_CONTRIB_USER_MANDELBROTMULTITHREAD)/build/$(BUILD_TYPE) ] || \
		mkdir -p $(MODULE_CONTRIB_USER_MANDELBROTMULTITHREAD)/build/$(BUILD_TYPE) || \
		exit 1
	@cd $(MODULE_CONTRIB_USER_MANDELBROTMULTITHREAD)/build/$(BUILD_TYPE) && \
		CC=$(CC) CFLAGS="$(TARGET_FLAGS)" \
		CXX=$(CXX) CXXFLAGS="$(TARGET_FLAGS)" \
		cmake $(CMAKE_COMMON_OPTIONS) ../.. || \
		exit 1
	@cd $(MODULE_CONTRIB_USER_MANDELBROTMULTITHREAD)/build/$(BUILD_TYPE) && \
		make -j$(CPUS) install || \
		exit 1

clean_mandelbrotmultithread:
	@echo
	@echo "==== Clean-up MandelbrotMultiThread Application ===="
	@[ ! -f $(BUILD_DIR)/usr/bin/mandelbrotmultithread ] || \
		rm -f $(BUILD_DIR)/etc/bbque/recipes/MandelbrotMultiThread*; \
		rm -f $(BUILD_DIR)/usr/bin/mandelbrotmultithread*
	@rm -rf $(MODULE_CONTRIB_USER_MANDELBROTMULTITHREAD)/build
	@echo

else # CONFIG_CONTRIB_MANDELBROTMULTITHREAD

mandelbrotmultithread:
	$(warning contib module MandelbrotMultiThread disabled by BOSP configuration)
	$(error BOSP compilation failed)

endif # CONFIG_CONTRIB_MANDELBROTMULTITHREAD

