/**
 *      @file  MandelbrotMultiThread_exc.h
 *      @brief  The MandelbrotMultiThread BarbequeRTRM application
 *
 * Description: Progetto di Piattaforme Software per la rete
 *              A simple algorithm to generate a representation of the Mandelbrot set based on the "escape time algorithm", adapted to work with BarbequeRTRM framework
 *
 *     @author  Ahmad El Bayoumi, medelbayoumi@gmail.com
 *     @author  Carmelo Fiorello, carmelofiorello@gmail.com
 *
 *     Company  Politecnico of Milan
 *     Copyright  Copyright (c) 2017, Carmelo Fiorello, Ahmad El Bayoumi
 *
 * This source code is released for free distribution under the terms of the
 * GNU General Public License as published by the Free Software Foundation.
 * =====================================================================================
 */

#ifndef MANDELBROTMULTITHREAD_EXC_H_
#define MANDELBROTMULTITHREAD_EXC_H_

#include <bbque/bbque_exc.h>
#include <iostream>
#include <opencv2/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <vector>
#include <bbque/utils/threadpool.h>
#include <thread>
#include <mutex>
#include <condition_variable>

using bbque::rtlib::BbqueEXC;
using namespace std;
using namespace cv;

class Lifo {

public:

/* Buffer Lifo of Ranges of Interest */
std::vector<std::pair<int,int> > consumables;

std::mutex lifo_mtx;

std::condition_variable lifo_cv, pushORstopP_cv;


/*
 * @brief Inserts an element into the buffer
 */
void Push(std::pair<int,int> element) {

        /* Acquire the lock */
        std::unique_lock<std::mutex> lock(lifo_mtx);

        /* Add an element and notify it */
        consumables.push_back(element);

        /* Release condition variable */
        lifo_cv.notify_one();

        /* Notify onMonitor that the producer has pushed validRange */
        pushORstopP_cv.notify_one();

}


/**
 * @brief Pops and returns the last element from the buffer
 */

std::pair<int,int> Pop(){

        /* Acquire the lock or wait in case of this is already Locked */
        std::unique_lock<std::mutex> lock(lifo_mtx);

        /* If the buffer is empty, wait */
        if (consumables.size() == 0)
                lifo_cv.wait(lock); //lock if condition variable aren't received notifies of Push

        /* Extract the element */
        std::pair<int,int> result = consumables.back();
        consumables.pop_back();

        /* Return the element */
        return result;
}

};

class MandelbrotMultiThread : public BbqueEXC {


Mat mandelbrotImg;
float x1, x2;
float y1, y2;
float scaleX;
float scaleY;

// In this LiFo the asynchronous thread pushes the ranges to be processed by the Threadpool  threads
Lifo lifo;

bool stopProducer, parallelDone, sequentialDone;

// The asyncronous thread
std::thread async_thread;

// The synchronous thread pool
ThreadPool sync_threadpool;

// The number of threads activated in the onRun()
int sync_threads_number;

int32_t proc_quota;

// The max number of iterations of the cycle in mandelbrot() function
int maxIterations;

//Max number of thread to start  (selected by the user)
int numThreads;

//Base image color (optional, 0=GREYSCALE, 1+BLUE, 2=RED, 3=GREEN)
int color;

//Image width
int width;

//operation mode of the algorithm - Default=0 - 0=parallel, 1=parallel+sequential
int mode;

// execution time in parallel and sequential mode
double tParallel, tSequential;

// vector storing the time between every onConfigure() and the value of
// vector storing for every time the process goes into onConfigure() the time passed since the last onConfigure() and the new value of the variable maxIterations
std::vector<std::pair<double,double>> history;

public:

MandelbrotMultiThread(std::string const & name, int numThreads, int color, int width, int mode,
                      std::string const & recipe,
                      RTLIB_Services_t *rtlib);

private:



RTLIB_ExitCode_t onSetup();
RTLIB_ExitCode_t onConfigure(int8_t awm_id);
RTLIB_ExitCode_t onRun();
RTLIB_ExitCode_t onMonitor();
RTLIB_ExitCode_t onSuspend();
RTLIB_ExitCode_t onRelease();

// escapeTime algorithm
int mandelbrot(const complex<float> &z0);

// mandelbrot process
int mandelbrotFormula(const complex<float> &z0);

// Function executed by each thread of the threadpool
void parallelThread();

// Function executed by the asynchronous thread
void setRanges();

// Sequential implementation of the algorithm
void sequentialProcess();

// Setting the maxIterations value dinamycally in the onConfigure() function
void setDynamicAccuracy();

// Function that takes as parameters the coordinates of the pixel and colors it (based on the user choice)
void colorPixel(int value, int i, int j);

// Function that pushes in the history vector the actual time since the beginning of the execution and the actual value of maxIterations
void pushHistory();

/*
 * Function that prints to the user the summary of the precision change in the execution.
 * It is called in the onRelease() phase
 *
 */
void printHistory();

};

#endif // MANDELBROTMULTITHREAD_EXC_H_
