/**
 *      @file  MandelbrotMultiThread_exc.cc
 *      @brief  The MandelbrotMultiThread BarbequeRTRM application
 *
 * Description: Progetto di Piattaforme Software per la rete
 *              A simple algorithm to generate a representation of the Mandelbrot set based on the "escape time algorithm", adapted to work with BarbequeRTRM framework
 *
 *     @author  Ahmad El Bayoumi, medelbayoumi@gmail.com
 *     @author  Carmelo Fiorello, carmelofiorello@gmail.com
 *
 *     Company  Politecnico of Milan
 *     Copyright  Copyright (c) 2017, Carmelo Fiorello, Ahmad El Bayoumi
 *
 * This source code is released for free distribution under the terms of the
 * GNU General Public License as published by the Free Software Foundation.
 * =====================================================================================
 */


#include "MandelbrotMultiThread_exc.h"

#include <cstdio>
#include <bbque/utils/utility.h>
#include <iostream>
#include <opencv2/core.hpp>
#include <opencv2/imgcodecs.hpp>

using namespace std;
using namespace cv;

//Only for test = represent number of remain iterations
int toDo;


int MandelbrotMultiThread::mandelbrot(const complex<float> &z0)
{
        complex<float> z = z0;
        for (int t = 0; t < maxIterations; t++)
        {
                if (z.real()*z.real() + z.imag()*z.imag() > 4.0f) return t;
                z = z*z + z0;
        }

        return maxIterations;
}


int MandelbrotMultiThread::mandelbrotFormula(const complex<float> &z0) {
        int value = mandelbrot(z0);
        if(maxIterations - value == 0)
        {
                return 0;
        }

        return cvRound(sqrt(value / (float) maxIterations) * 255);
}


void MandelbrotMultiThread::parallelThread() {

        std::pair<int,int> popElement = lifo.Pop();

        int start = popElement.first;
        int end = popElement.second;

        logger->Info("Inizio Thread del pool, ranges: %d - %d", start, end);

        for (int r = start; r < end; r++) {
                int i = r / mandelbrotImg.cols;
                int j = r % mandelbrotImg.cols;

                float x0 = j / scaleX + x1;
                float y0 = i / scaleY + y1;

                complex<float> z0(x0, y0);
                uchar value = (uchar) mandelbrotFormula(z0);

                colorPixel(value, i, j);
        }

}


void MandelbrotMultiThread::setRanges() {

        logger->Notice("MandelbrotMultiThread::setRanges(): Beginning");

        int pixels = mandelbrotImg.cols * mandelbrotImg.rows;

        int range = mandelbrotImg.cols;
        int start = 0;
        int end = 0;

        while (end != pixels) {
                end = start + range;
                if (end > pixels) {
                        end = pixels;
                }
                std::pair<int,int> pushElement = std::make_pair(start,end);
                lifo.Push(pushElement);
                start = end+1;
        }

        stopProducer = true;

        /* Notify onMonitor that the producer has finished */
        lifo.pushORstopP_cv.notify_one();
        toDo = lifo.consumables.size();
        logger->Notice("MandelbrotMultiThread::setRanges(): Stopping");
        logger->Notice("MandelbrotMultiThread::setRanges(): Pushed %d ranges", lifo.consumables.size());

}


void MandelbrotMultiThread::setDynamicAccuracy() {

        maxIterations = proc_quota * 25;

}


void MandelbrotMultiThread::sequentialProcess()
{
        logger->Notice("Sequential thread beginning...");

        tSequential = (double) getTickCount();

        for (int i = 0; i < mandelbrotImg.rows; i++)
        {
                //logger->Warn("I = %d / ROWS = %d...",i, mandelbrotImg.rows);
                for (int j = 0; j < mandelbrotImg.cols; j++)
                {
                        float x0 = j / scaleX + x1;
                        float y0 = i / scaleY + y1;

                        complex<float> z0(x0, y0);
                        uchar value = (uchar) mandelbrotFormula(z0);

                        colorPixel(value, i, j);
                }
        }

        tSequential = ((double) getTickCount() - tSequential) / getTickFrequency();

        logger->Notice("Sequential execution time: %f", tSequential);

        sequentialDone = true;
}

// ------------------------------------------------------------------------

MandelbrotMultiThread::MandelbrotMultiThread(std::string const & name, int numThreads, int color, int width, int mode,
                                             std::string const & recipe,
                                             RTLIB_Services_t *rtlib) :
        BbqueEXC(name, recipe, rtlib) {

        this->numThreads = numThreads;
        this->color = color;
        this->width = width;
        if(mode != 0){
            this->mode = mode;
        }
        else{
            this->mode = 0;
        }
        logger->Warn("Numero di thread, colore e width settati");

}

RTLIB_ExitCode_t MandelbrotMultiThread::onSetup() {

        logger->Warn("MandelbrotMultiThread::onSetup()");

        stopProducer = false;
        sequentialDone = false;
        parallelDone = false;

        // Creating Mat image with 3 channels for pixel(RGB)
        if(color != 0) {
                mandelbrotImg.create(width/1.25, width, CV_8UC3);
        }
        // Creating Mat image single channel for grey-scale
        else{
                mandelbrotImg.create(width/1.25, width, CV_8U);
        }

        x1 = -2.1f, x2 = 0.6f;
        y1 = -1.2f, y2 = 1.2f;
        scaleX = mandelbrotImg.cols / (x2 - x1);
        scaleY = mandelbrotImg.rows / (y2 - y1);

        /* Starting the asynchronous thread */
        logger->Notice("Launching asynchronous thread");

        tParallel = (double) getTickCount();

        async_thread = std::thread(&MandelbrotMultiThread::setRanges, this);

        /*
           Setting up the synchronous threads.
           The ThreadPool size is set to the value inserted by the user
         */
        logger->Notice("Threadpool setup");

        //sync_threadpool.Setup((int)std::thread::hardware_concurrency(), std::bind(&MandelbrotMultiThread::parallelThread, this));
        sync_threadpool.Setup(numThreads, std::bind(&MandelbrotMultiThread::parallelThread, this));

        return RTLIB_OK;
}

RTLIB_ExitCode_t MandelbrotMultiThread::onConfigure(int8_t awm_id) {

        logger->Warn("MandelbrotMultiThread::onConfigure(): EXC [%s] => AWM [%02d]",
                     exc_name.c_str(), awm_id);

        int32_t mem, proc_nr;
        GetAssignedResources(PROC_ELEMENT, proc_quota);
        GetAssignedResources(PROC_NR, proc_nr);
        GetAssignedResources(MEMORY, mem);
        logger->Notice("MayApp::onConfigure(): "
                       "EXC [%s], AWM[%02d] => R<PROC_quota>=%3d, R<PROC_nr>=%2d, R<MEM>=%3d",
                       exc_name.c_str(), awm_id, proc_quota, proc_nr, mem);

        // Number of Threadpool's threads setted to the minimum value between the user choice and the threads available
        this->numThreads = std::min (proc_nr, this->numThreads);

        // Limit the Threadpool's threads number to the the size of the ranges in the LIFO in the case that the they are less than the available threads
        sync_threads_number =  std::min ((int)lifo.consumables.size(), this->numThreads);

        pushHistory();

        // Dynamically set maxIterations value
        setDynamicAccuracy();

        return RTLIB_OK;
}

RTLIB_ExitCode_t MandelbrotMultiThread::onRun() {
        RTLIB_WorkingModeParams_t const wmp = WorkingModeParams();

        logger->Notice("MandelbrotMultiThread::onRun()");

        // When parallel execution is done, if mode!=0 the sequential process will be executed, else not
        if (parallelDone) {
                if (mode == 0) { //only parallel
                        sequentialDone = true;
                        return RTLIB_OK;
                }
                else { //parallel and sequential
                        sequentialProcess();
                        return RTLIB_OK;
                }
        }


        // If the producer has not finished running, the threadpool will run the setted number of threads - 1
        if(!stopProducer && (sync_threads_number == this->numThreads)) sync_threads_number -= 1;

        logger->Notice("Starting pool with %d threads, remain %d ranges to elaborate", sync_threads_number, toDo);
        toDo = toDo - 2;

        // Start sync_threads_number threads of ThreadPool
        sync_threadpool.Start(sync_threads_number);

        return RTLIB_OK;
}

RTLIB_ExitCode_t MandelbrotMultiThread::onMonitor() {
        RTLIB_WorkingModeParams_t const wmp = WorkingModeParams();



        sync_threads_number = std::min ((int)lifo.consumables.size(), this->numThreads);

        // If there are no valid ranges and the producer has not finished, Wait for push or stopProducer
        if((sync_threads_number == 0) && !stopProducer) {

                logger->Warn("onMonitor(), waiting for asynchronous thread to finish");

                std::unique_lock<std::mutex> lock(lifo.lifo_mtx);
                lifo.pushORstopP_cv.wait(lock);

                // Updates the number of threads (consistent with the notify) that will run in the next cycle
                sync_threads_number = std::min ((int)lifo.consumables.size(), this->numThreads);
        }

        // Stop condition. When there are no more jobs to do and the producer ended
        if ((sync_threads_number == 0) && stopProducer) {
                logger->Notice("MandelbrotMultiThread::onMonitor()  : ParallelWork Done");
                tParallel = ((double) getTickCount() - tParallel) / getTickFrequency();
                logger->Notice("Parallel execution time: %f", tParallel);
                pushHistory();
                parallelDone = true;
                imwrite("Mandelbrot_bospparallel.png", mandelbrotImg);
                if (this->mode == 0) {
                  return RTLIB_EXC_WORKLOAD_NONE;
                }
        }
        // When sequential is done
        if (sequentialDone && this->mode != 0) {
                logger->Notice("Sequential Processing finished");
                imwrite("Mandelbrot_bosp_sequential.png", mandelbrotImg);
                return RTLIB_EXC_WORKLOAD_NONE;
        }

        return RTLIB_OK;
}

RTLIB_ExitCode_t MandelbrotMultiThread::onSuspend() {

        logger->Warn("MandelbrotMultiThread::onSuspend()  : suspension...");

        return RTLIB_OK;
}

RTLIB_ExitCode_t MandelbrotMultiThread::onRelease() {

        logger->Warn("MandelbrotMultiThread::onRelease()  : exit");

        logger->Info("MandelbrotMultiThread::onRelease()  : waiting for async thread to stop\n");
        async_thread.join();

        logger->Info("MandelbrotMultiThread::onRelease()  : waiting for sync threads to stop\n");
        sync_threadpool.Release();


        logger->Notice("****************************************************************************************************");

        logger->Notice("Parallel execution time: %f", tParallel);
        if(mode != 0) {
          logger->Notice("Sequential execution time: %f", tSequential);

          logger->Warn("Speed-up: %f", tSequential/tParallel);

        }

        logger->Notice("****************************************************************************************************");

        printHistory();
        return RTLIB_OK;
}



void MandelbrotMultiThread::colorPixel(int value, int i, int j) {

  if (color == 3) //GREEN
  {
          mandelbrotImg.at<Vec3b>(i,j)[0]=1;
          mandelbrotImg.at<Vec3b>(i,j)[1]=value;
          mandelbrotImg.at<Vec3b>(i,j)[2]=1;
  }
  else if (color == 1) //BLUE
  {
          mandelbrotImg.at<Vec3b>(i,j)[0]=value;
          mandelbrotImg.at<Vec3b>(i,j)[1]=1;
          mandelbrotImg.at<Vec3b>(i,j)[2]=1;
  }
  else if (color == 2) //RED
  {
          mandelbrotImg.at<Vec3b>(i,j)[0]=1;
          mandelbrotImg.at<Vec3b>(i,j)[1]=1;
          mandelbrotImg.at<Vec3b>(i,j)[2]=value;
  }
  else //GREYSCALE
  {
          mandelbrotImg.ptr<uchar>(i)[j] = value;
  }

}


void MandelbrotMultiThread::pushHistory() {

  double time = (double) getTickCount() / getTickFrequency();

  std::pair<int,int> pushElement = std::make_pair(maxIterations, time);

  history.push_back(pushElement);

}


void MandelbrotMultiThread::printHistory() {

  int t = 0;
  for (auto&& i : history) {
    if(t != 0) {
      logger->Warn("onConfigure n.%d : maxIter was %f for %f s", t, i.first, i.second - (history.at(t-1).second));
    }
    t++;
  }


}
