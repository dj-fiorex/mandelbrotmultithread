# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/melo/BOSP/contrib/user/MandelbrotMultiThread/src/MandelbrotMultiThread_exc.cc" "/home/melo/BOSP/contrib/user/MandelbrotMultiThread/build/Release/src/CMakeFiles/mandelbrotmultithread.dir/MandelbrotMultiThread_exc.cc.o"
  "/home/melo/BOSP/contrib/user/MandelbrotMultiThread/src/MandelbrotMultiThread_main.cc" "/home/melo/BOSP/contrib/user/MandelbrotMultiThread/build/Release/src/CMakeFiles/mandelbrotmultithread.dir/MandelbrotMultiThread_main.cc.o"
  "/home/melo/BOSP/contrib/user/MandelbrotMultiThread/build/Release/src/version.cc" "/home/melo/BOSP/contrib/user/MandelbrotMultiThread/build/Release/src/CMakeFiles/mandelbrotmultithread.dir/version.cc.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "UNIX"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "include"
  "../../include"
  "/home/melo/BOSP/out/include"
  "/usr/local/include"
  "/usr/local/include/opencv"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
