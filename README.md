# Progetto Piattaforme Software per la rete
## Mandelbrot Benchmark
###### Carmelo Fiorello 806773
###### Ahmad El Bayoumi 811150
###### Per la documentazione relativa all'app e ai test effettuati riferirsi al file README.pdf all'interno della cartella README
###### Nella cartella README/ImmaginiTest si trovano tutte le immagini che l'applicazione ha prodotto durante i test.
###### Il nome del file ha questo significato: Mandelbrot_Accuratezza_Width_Colore
###### Con colore: G = Greyscale, V = Verde, B = Blu, R = Rosso